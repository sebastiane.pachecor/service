package com.conexia.pruebasws;

import org.junit.Assert;
import org.junit.Test;

import static com.conexia.pruebasws.ClaveUtil.NivelSeguridad.*;

public class ClaveUtilTest {

    @Test
    public void debil_cuando_un_password_tiene_menos_de_ocho_letras() {
        Assert.assertEquals(DEBIL,ClaveUtil.validarClave("12md!`"));
    }

    @Test
    public void debil_cuando_un_password_solo_tiene_letras() {
        Assert.assertEquals(DEBIL,ClaveUtil.validarClave("abcdefghjhgfkhjgfjkhgf"));
    }

    @Test
    public void medio_cuando_un_password_tiene_letras_y_numeros() {
        Assert.assertEquals(MEDIO,ClaveUtil.validarClave("abcdefghjgfdjhgdh12"));
    }

    @Test
    public void medio_cuando_un_password_tiene_letras_numeros_y_simbolos() {
        Assert.assertEquals(ALTO,ClaveUtil.validarClave("abcdefgh12!"));
    }
}