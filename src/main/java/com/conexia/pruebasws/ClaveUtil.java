package com.conexia.pruebasws;

public class ClaveUtil {

    public enum NivelSeguridad{DEBIL, MEDIO, ALTO};

    public static NivelSeguridad validarClave(String clave){
        if(clave.length() < 8 || clave.matches("[a-zA-Z]+")){
            return NivelSeguridad.DEBIL;
        }
        if(clave.matches("[a-zA-Z0-9]+")){
            return NivelSeguridad.MEDIO;
        }
        return NivelSeguridad.ALTO;
    }
}
