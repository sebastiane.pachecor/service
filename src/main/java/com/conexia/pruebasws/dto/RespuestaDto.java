package com.conexia.pruebasws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RespuestaDto {

    @JsonProperty("Valor")
    private Integer valor;

    @JsonProperty("Mensaje")
    private String mensaje;

    public RespuestaDto(Integer valor, String mensaje) {
        this.valor = valor;
        this.mensaje = mensaje;
    }
}
