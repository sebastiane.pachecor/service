package com.conexia.pruebasws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Builder
public @Data class AfiliadoDto implements Serializable {

    @JsonProperty("IDExternoAfiliado")
    private Integer idExternoAfiliado;

    @JsonProperty("TipoIdentificacion")
    private String tipoIdentificacion;

    @JsonProperty("NumeroIdentificacion")
    private String numeroIdentificacion;

    @JsonProperty("NombreAcompanante")
    private String nombreAcompaniante;

    @JsonProperty("TelefonoFijoAcompanante")
    private String telefonoFijoAcompanante;

    @JsonProperty("TelefonoCelularAcompanante")
    private String telefonoCelularAcompanante;
}
