package com.conexia.pruebasws.repository;

import com.conexia.pruebasws.model.AfiliadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AfiliadoRepository extends JpaRepository<AfiliadoEntity, Integer> {

}
