package com.conexia.pruebasws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.conexia.pruebasws")
@EntityScan("com.conexia.pruebasws")
@SpringBootApplication
public class PruebaswsApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder){
		return builder.sources(PruebaswsApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PruebaswsApplication.class, args);
	}

}
