package com.conexia.pruebasws.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "afiliado")
public class AfiliadoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_externo_afiliado")
    private Integer idExternoAfiliado;

    @Column(name = "tipo_identificacion")
    private String tipoIdentificacion;

    @Column(name = "numero_identificacion")
    private String numeroIdentificacion;

    @Column(name = "nombre_acompañante")
    private String nombreAcompañante;

    @Column(name = "telefono_celular_acompanante")
    private String telefonoCelularAcompanante;
}
