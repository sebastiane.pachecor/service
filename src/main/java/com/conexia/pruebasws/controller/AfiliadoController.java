package com.conexia.pruebasws.controller;

import com.conexia.pruebasws.dto.AfiliadoDto;
import com.conexia.pruebasws.service.interfaces.AfiliadoInterface;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/afiliados")
public class AfiliadoController {

    private final AfiliadoInterface afiliadoInterface;

    @GetMapping(value = "/obtenerAfiliado", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> obtenerAfiliado(@RequestParam Integer afiliadoId) throws JsonProcessingException {
        return new ResponseEntity<>(afiliadoInterface.obtenerAfiliado(afiliadoId), HttpStatus.OK);
    }

    @PostMapping("/registrarAfiliado")
    public ResponseEntity<String> obtenerAfiliado(@RequestBody AfiliadoDto afiliadoDto){
        return new ResponseEntity<>(afiliadoInterface.registrar(afiliadoDto), HttpStatus.OK);
    }
}
