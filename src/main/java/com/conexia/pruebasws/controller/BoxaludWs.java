package com.conexia.pruebasws.controller;

import com.conexia.pruebasws.dto.AfiliadoDto;
import com.conexia.pruebasws.dto.RespuestaDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.Properties;

@Slf4j
@RestController
public class BoxaludWs {

    @Value("${archivo.logj4}")
    private String directorio;

    private static final Logger logger = Logger.getLogger("Logger");

    private FileInputStream file;

    private Properties properties;

    @PostConstruct
    public void construirLog() throws IOException {
        properties = new Properties();
        file = new FileInputStream(directorio);
        properties.load(file);
        properties.setProperty("log4j.appender.Logger.File","/home/spacheco/log/BaseDatos.log");
        properties.store(new FileWriter(directorio),"Actualizacion");
        PropertyConfigurator.configure(directorio);
        logger.debug("this is a debug log message");
        logger.info("this is a information log message");
        logger.warn("this is a warning log message");
    }

    @RequestMapping(value="/", method= RequestMethod.GET)
    public String init(){
        logger.debug("this is a debug log message");
        logger.info("this is a information log message");
        logger.warn("this is a warning log message");
        return "Proyecto Arriba!!";
    }

    @RequestMapping(value="/desactivar", method= RequestMethod.GET)
    public String desactivar() throws IOException {
        properties.setProperty("log4j.logger.Logger","OFF,Logger");
        properties.store(new FileWriter(directorio),"Actualizacion");
        PropertyConfigurator.configure(directorio);
        logger.debug("this is a debug log message");
        logger.info("this is a information log message");
        logger.warn("this is a warning log message");
        return "Desactivado!!";
    }

    @PostMapping("/actualizarAfiliado")
    public ResponseEntity actualizarAfiliado(@RequestBody AfiliadoDto afiliadoDto){
        logger.info("se recibe petición de actualización "+afiliadoDto.toString());
        return ResponseEntity.ok(new RespuestaDto(1,"Error el telefono es mio"));
        //return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new RespuestaDto(500,"Error Internal server error!!"));
    }

}
