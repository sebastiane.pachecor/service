package com.conexia.pruebasws.service.interfaces;

import com.conexia.pruebasws.dto.AfiliadoDto;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface AfiliadoInterface {

    String registrar(AfiliadoDto afiliadoDto);

    String obtenerAfiliado(Integer afiliadoId) throws JsonProcessingException;
}
