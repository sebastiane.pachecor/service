package com.conexia.pruebasws.service.implement;

import com.conexia.pruebasws.dto.AfiliadoDto;
import com.conexia.pruebasws.model.AfiliadoEntity;
import com.conexia.pruebasws.repository.AfiliadoRepository;
import com.conexia.pruebasws.service.interfaces.AfiliadoInterface;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AfiliadoImplement implements AfiliadoInterface {

    private final AfiliadoRepository afiliadoRepository;

    @Override
    public String registrar(AfiliadoDto afiliadoDto) {
        AfiliadoEntity afiliadoEntity = AfiliadoEntity.builder()
                    .idExternoAfiliado(afiliadoDto.getIdExternoAfiliado())
                    .tipoIdentificacion(afiliadoDto.getTipoIdentificacion())
                    .nombreAcompañante(afiliadoDto.getNombreAcompaniante())
                    .numeroIdentificacion(afiliadoDto.getNumeroIdentificacion())
                    .telefonoCelularAcompanante(afiliadoDto.getTelefonoCelularAcompanante())
                    .build();
        afiliadoEntity = afiliadoRepository.save(afiliadoEntity);
        return afiliadoEntity.toString();
    }

    @Override
    public String obtenerAfiliado(Integer afiliadoId) throws JsonProcessingException {
        AfiliadoEntity afiliadoEntity = afiliadoRepository.findById(afiliadoId).orElse(null);
        return new ObjectMapper().writeValueAsString(afiliadoEntity);
    }
}
